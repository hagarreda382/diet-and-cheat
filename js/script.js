$(window).ready(function () {
  // Preloader
  $(".loader").delay(3000).fadeOut();
});

function typeEffect(element, speed) {
  var text = element.innerHTML;
  element.innerHTML = "";

  var i = 0;
  var timer = setInterval(function () {
    if (i < text.length) {
      element.append(text.charAt(i));
      i++;
    } else {
      clearInterval(timer);
    }
  }, speed);
}

// application
var speed = 75;
var head = document.querySelector(".text-load h2");
var pragraph = document.querySelector(".text-load p");
var delay = head.innerHTML.length * speed + speed;

// type affect to header
typeEffect(head, speed);

// type affect to body
setTimeout(function () {
  pragraph.style.display = "inline-block";
  typeEffect(pragraph, speed);
}, delay);

// sticky header
$(document).ready(function () {
  if ($("#not-sticky").length != 0) {
    return;
  }
  var $sticky = $("header");
  if ($sticky.length === 0) return;
  var stickyOffsetTop = $sticky.offset().top;

  $(window).scroll(function (e) {
    e.preventDefault();

    var scrollTop = $(window).scrollTop();
    if (window.matchMedia("(min-width: 991px)").matches) {
      if (scrollTop > stickyOffsetTop) {
        $sticky.addClass("sticky-navbar");
      } else {
        $sticky.removeClass("sticky-navbar");
      }
    } else {
      $sticky.addClass("sticky-navbar");
    }
  });
});

//=======================gallery-top=======================
var galleryTop = new Swiper(".gallery-top", {
  loop: true,
  autoplay: {
    delay: 3000,
  },
  pagination: {
    el: ".gallery-top .swiper-pagination",
    clickable: true,
    renderBullet: function (index, className) {
      return `<span class=${className} >tab ${index + 1}</span>`;
    },
  },
});

//==============swiper-about================
$(document).ready(function () {
  var $swiper = $(".swiper-about .swiper-container");
  var $bottomSlide = null;

  var $bottomSlideContent = null;

  var mySwiper = new Swiper(".swiper-about .swiper-container", {
    spaceBetween: 1,
    slidesPerView: 3,
    centeredSlides: true,
    roundLengths: true,
    loop: false,
    loopAdditionalSlides: 30,
    autoplay: {
      delay: 2000,
      disableOnInteraction: false
    },
    navigation: {
      nextEl: ".swiper-about .swiper-button-next",
      prevEl: ".swiper-about .swiper-button-prev",
    },
    pagination: {
      el: ".swiper-about  .swiper-pagination",
      clickable: true,
    },
  });
});

//===========swiper-stories=============================
$(function () {
  const swiper = new Swiper(".swiper-stories .swiper-container", {
    spaceBetween: 20,
    slidesPerView: 1,
    mousewheel: false,
    keyboard: {
      enabled: true
    },
    breakpoints: {
      500: {
        slidesPerView: 2,
      },
      767: {
        slidesPerView: 3,
      },

      1200: {
        slidesPerView: 4,
      },
    },

    autoplay: {
      delay: 2000,
      disableOnInteraction: false
    },

    loop: false,

    pagination: {
      el: ".swiper-stories  .swiper-pagination",
      type: "bullets",
      clickable: true,
    },

    navigation: {
      nextEl: ".swiper-stories .swiper-button-next",
      prevEl: ".swiper-stories .swiper-button-prev",
    },
  });
});

//=======================swiper-test======================
$(function () {
  const swiper = new Swiper(".swiper-test .swiper-container", {
    spaceBetween: 20,
    slidesPerView: 1,
    mousewheel: false,
    keyboard: {
      enabled: true
    },
    breakpoints: {
      500: {
        slidesPerView: 1,
      },
      767: {
        slidesPerView: 2,
      },

      1200: {
        slidesPerView: 3,
      },
    },

    autoplay: {
      delay: 2000,
      disableOnInteraction: false
    },

    loop: false,

    pagination: {
      el: ".swiper-test .swiper-pagination",
      type: "bullets",
      clickable: true,
    },

    navigation: {
      nextEl: ".swiper-test .swiper-button-next",
      prevEl: ".swiper-test .swiper-button-prev",
    },
  });
});

//====================swiper-case======================
$(function () {
  const swiper = new Swiper(".swiper-case .swiper-container", {
    spaceBetween: 20,
    slidesPerView: 1,
    mousewheel: false,
    keyboard: {
      enabled: true
    },
    breakpoints: {
      500: {
        slidesPerView: 1,
      },
      992: {
        slidesPerView: 2,
      },
    },

    autoplay: {
      delay: 2000,
      disableOnInteraction: false
    },

    loop: false,

    pagination: {
      el: ".swiper-case  .swiper-pagination",
      type: "bullets",
      clickable: true,
    },

    navigation: {
      nextEl: ".swiper-case .swiper-button-next",
      prevEl: ".swiper-case .swiper-button-prev",
    },
  });
});

//============================swiper-containerPartners====================
var swiper = new Swiper(".swiper-containerPartners", {
  direction: "horizontal",
  loop: true,
  slidesPerView: 2,
  simulateTouch: true,
  effect: "slide",
  autoplay: {
    enabled: true,
    delay: 0,
    disableOnInteraction: false,
    pauseOnMouseEnter: false,
  },
  centerInsufficientSlides: true,
  speed: 3000,
  breakpoints: {
    280: {
      slidesPerView: 3,
    },
    500: {
      slidesPerView: 4,
    },
    992: {
      slidesPerView: 3,
    },
  },
});

//=======================countdown=====================
// (function () {
//   const second = 1000,
//     minute = second * 60,
//     hour = minute * 60,
//     day = hour * 24;

//   let today = new Date(),
//     yyyy = today.getFullYear(),
//     dayMonth = "10/11/",
//     birthday = dayMonth + yyyy;

//   const countDown = new Date(birthday).getTime(),
//     x = setInterval(function () {
//       const now = new Date().getTime(),
//         distance = countDown - now;

//       (document.getElementById("days") = Math.floor(distance / day)),
//         (document.getElementById("hours").innerText = Math.floor(
//           (distance % day) / hour
//         )),
//         (document.getElementById("minutes").innerText = Math.floor(
//           (distance % hour) / minute
//         )),
//         (document.getElementById("seconds").innerText = Math.floor(
//           (distance % minute) / second
//         ));
//     }, 0);
// })();

//===============================radio==========================


let button1 = document.querySelector(".accordion-body #button1");
let button2 = document.querySelector(".accordion-body #button2");
let div1 = document.querySelector(".accordion-body #div1");
let div2 = document.querySelector(".accordion-body #div2");

button1 &&
  button1.addEventListener("click", () => {
    div1.classList.add("active");
    div2.classList.remove("active");
    button1.classList.add("active-button");
    button2.classList.remove("active-button");
  });

button2 &&
  button2.addEventListener("click", () => {
    div2.classList.add("active");
    div1.classList.remove("active");
    button2.classList.add("active-button");
    button1.classList.remove("active-button");
  });

//=====================================h1===================


//===============play-vedio========================
// let vedioTrain = document.querySelector(".v-training");
// let currentVideoStatus = false;
// vedioTrain.addEventListener("click", function () {
//   !currentVideoStatus ? vedioTrain.play() : vedioTrain.pause();
//   currentVideoStatus = !currentVideoStatus;
//   console.log("add event one fjetjpgkp");
// });

(function () {
  let videos = document.querySelectorAll(".v-training");
  let videoimgs = document.querySelectorAll(".img-frame2 img");
  let playIcon = document.querySelectorAll(".fancybox-link");

  videoimgs.forEach((videoimg, index) => {
    videoimg &&
      videoimg.addEventListener("click", function () {
        videos[index].paused ? videos[index].play() : videos[index].pause();
      });

    videos[index] &&
      videos[index].addEventListener("play", function () {
        playIcon[index].style.opacity = "0";
      });

    videos[index] &&
      videos[index].addEventListener("pause", function () {
        playIcon[index].style.opacity = "1";
      });
  });
})();

//==============================hide=================================
(function () {
 
  let accordionHeaders = document.querySelectorAll('.accord-headinside1 .accordion-button');


accordionHeaders.forEach((accordionHeader) => {
  accordionHeader &&   accordionHeader.addEventListener('click', () => {
    let expanded = accordionHeader.getAttribute('aria-expanded') === 'true';
    let contentElement = accordionHeader.querySelector('.textPayment');
    let imgHide = accordionHeader.querySelector('.img-hide');
    if (expanded) {
      contentElement.style.display = 'block';
      imgHide.style.display = 'none';
      accordionHeader.setAttribute('aria-expanded', 'false');
    } else {
      contentElement.style.display = 'none';
      imgHide.style.display = 'inline-block';
    
   
      accordionHeader.setAttribute('aria-expanded', 'true');
    }
  });
});





})();


//==================radio checked========================


const radioContainers = document.querySelectorAll('.accord-button');

radioContainers.forEach((container, index) => {
  const radios = container.querySelectorAll('.form-check-input');

  radios.forEach((radio) => {
    radio.addEventListener('change', () => {
      $(".form-check").css("borderColor", '');
      $(".accord-button .accordion-collapse").removeClass("show")
      $(radio).closest('.form-check').css("borderColor",'#efaa50');
    });
  });
});



//=====================scroll=========================

let scroll = document.querySelector(".scroll");
let isDown = false;
let scrollX;
let scrollLeft;

// Mouse Up Function
scroll && scroll.addEventListener("mouseup", () => {
	isDown = false;
	scroll.classList.remove("active");
});

// Mouse Leave Function
scroll && scroll.addEventListener("mouseleave", () => {
	isDown = false;
	scroll.classList.remove("active");
});

// Mouse Down Function
scroll && scroll.addEventListener("mousedown", (e) => {
	e.preventDefault();
	isDown = true;
	scroll.classList.add("active");
	scrollX = e.pageX - scroll.offsetLeft;
	scrollLeft = scroll.scrollLeft;
});

// Mouse Move Function
scroll && scroll.addEventListener("mousemove", (e) => {
	if (!isDown) return;
	e.preventDefault();
	var element = e.pageX - scroll.offsetLeft;
	var scrolling = (element - scrollX) * 2;
	scroll.scrollLeft = scrollLeft - scrolling;
});

//==========================================radio to show accordion====================================

function showAccordion(index) {
  let accordions = document.querySelectorAll(`._accordion`);
  let accordion = accordions[index -1]
  if (accordion) {
    let accordionContent = accordion.querySelector('.accordion-content');
    let collapseBtn = accordion.querySelector('.accordion-button-head')
    let isOpen = accordionContent.classList.contains('show');
    if (!isOpen) {
      collapseBtn.classList.remove("collapsed")
      accordionContent.classList.add('show');
    }
    window.scrollTo(0, accordion.getBoundingClientRect().top - 75)
  }
}




